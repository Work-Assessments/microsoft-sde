﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace Employee.Services
{
    public class Employees
    {
        private String filename ;
        private String delimiter;
        private Int16 rowCount;

        private Int16 managerCount;
        private Int16 EmployeeOccurence;

        private List<EmployeeItem> employeeData = new List<EmployeeItem>();

        public Employees(String filename, String delimiter)
        {
            List <EmployeeSalaries> EmployeeSalaries= new List<EmployeeSalaries> ();

            this.filename =filename;    
            this.delimiter =delimiter;     

            if (loadCsv(filename,delimiter) )
            {
                circularRefCheck();
                isEmployeeValid();
                EmployeeSalaries = getSalaries();
            }

        }

        public bool loadCsv(String filename,String delimiter)
        {
            using (TextFieldParser parser = new TextFieldParser(filename))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                parser.HasFieldsEnclosedInQuotes = false;
                parser.TrimWhiteSpace = true;
                while (parser.PeekChars(1) != null)
                {
                    //Get row data
                    string[] data_rows = parser.ReadFields().Select(f => f.Trim(new[] { ' ', '"' }));

                    if ( checkRowLengthAcceptable(rowCount, data_rows))
                    { 
                        // Get count 
                        isCEO(data_rows[1]);

                        Int16 salary=0;

                        if (checkSalaryInt(data_rows[2],salary))
                        {
                            employeeData.Add (addItemToList(data_rows[0],data_rows[1],salary) );
                        }           
                    }
                    else
                    {
                        // Throw error
                        error("unable to load csv");
                        return false;
                    }

                }
                return true;
            }
        }

        public bool circularRefCheck()
        {
            for (var i = 0; i < employeeData.Count; i++) 
            {
                if ( !string.IsNullOrEmpty(employeeData[i].manager))
                {
                    for (var j = 0; j < employeeData.Count; j++) 
                    {
                        if (employeeData[i].manager == employeeData[j].manager)
                        {
                            // Throw error report more than on manager
                            error("More than one manager");
                            return true;
                        }

                        if (employeeData[i].employee == employeeData[j].manager && employeeData[i].manager == employeeData[j].employee)
                        {
                            // Throw error circular ref
                            error("Circular ref");
                            return true;
                            
                        }
                    }
                }
            }
            return false;
        }

        public bool isEmployeeValid()
        {
            EmployeeOccurence =0;
            for (var i = 0; i < employeeData.Count; i++) 
            {
                if ( !string.IsNullOrEmpty(employeeData[i].manager))
                {
                    for (var j = 0; j < employeeData.Count; j++) 
                    {
                        if (employeeData[i].manager == employeeData[j].employee)
                        {
                            // Throw error
                            EmployeeOccurence++;
                            error("Employee error");
                            return false;
                        }
                    }
                }

                if(EmployeeOccurence != 1)
                {
                    // Throw Error manger not employee or multiple entry
                    error("manger not employee");
                    return false;
                }
            }
            return true;
        }

        public bool checkRowLengthAcceptable(Int16 count, Object [] array)
        {
            if (array.Length == count)
            {
                return true;
            }
            return false;
        }
        public bool checkSalaryInt(String salary, Int16 salary_int)
        {
            if(Int16.TryParse(salary, out salary_int))
            {
                return true;
            }
            return false;
        }

        public void isCEO(String manager)
        {
            if (string.IsNullOrEmpty(manager)) 
            {
                managerCount++;
            }
        }
        
        public EmployeeItem addItemToList(String empl, String mgr, Int16 sal)
        {
            EmployeeItem employee =new EmployeeItem();
            
            employee.employee =empl;
            employee.manager=mgr;
            employee.salary=sal;

            return employee;
        }

        public List<EmployeeSalaries> getSalaries()
        {
            List<String> managers = new List<String>();
            Int16 sum =0;

            List<EmployeeSalaries> salaries = new List<EmployeeSalaries>();

            foreach (String manager in managers) 
            {
                foreach(var employee_data in employeeData )
                {
                    if( employee_data.manager.ToString().Contains(manager.ToString()))
                    {
                        managers.Add(employee_data.manager);
                    }   
                }
            }
            foreach (var manager in managers) 
            {
                foreach (var employee in employeeData )
                {
                    if (employee.manager.ToString().Contains(manager.ToString()) || employee.employee.ToString().Contains(manager.ToString()) )
                    {
                        sum = (Int16) (sum + employee.salary);
                    }
                }
                
                EmployeeSalaries sal_grp = new EmployeeSalaries();
                sal_grp.salary = sum;
                sal_grp.manager = manager;
                salaries.Add(sal_grp);
                
                sum=0;
            }
            return salaries;
        }

        public void error(String error)
        {
            throw new System.ArgumentException("Error:",error);
        }

    }
}
