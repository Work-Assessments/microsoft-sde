using Microsoft.VisualStudio.TestTools.UnitTesting;

using Employee.Services;


namespace EmployeeService
{
    [TestClass]
    public class EmployeeTest
    {
        private readonly EmployeeService _employeeService;

        public EmployeeTest()
        {
            _employeeService = new EmployeeService();
        }

        [TestMethod]
        public void Test_isCEO()
        {
            _employeeService.isCEO("");
            Assert.AreEqual( _employeeService.managerm,0);

        }

        [TestMethod]
        public void Test_addItemToList()
        {
            EmployeeItem employee = new EmployeeItem();
            employee = _employeeService.Test_addItemToList("empl01","man_01",200);
            Assert.AreEqual(employee.manager,"man_01");
        }

        [TestMethod]
        public void Test_checkRowLengthAcceptable()
        {
            int[] test = new int[] { 1, 3, 5, 7};
            Assert.IsTrue (_employeeService.checkRowLengthAcceptable(5,test),5,"checkrowlength failed");
        }
    }
}
